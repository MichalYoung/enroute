#! /usr/local/bin/python3
# Measure time of operations used in time_test.py
#
from time import perf_counter, strftime

EVENTS = [ ]
def log_time(evname):
    global EVENTS
    event = (evname, perf_counter())
    EVENTS.append(event)
    return

def report_times():
    global EVENTS
    begin_event, begin_time = EVENTS[0]
    prior = begin_time
    for event in EVENTS[1:]:
        evname, evtime = event
        delta = evtime - prior
        elapsed = evtime - begin_time
        prior = evtime
        print("{}\t{:3f}  ({:3f})".format(evname, elapsed, delta))

log_time("begin")

import spot  # Interface to Spot satellite tracker web service
import gpx_load
import gmap

log_time("imports")



rwgps_url = "http://ridewithgps.com/routes/" + "5079973" + ".gpx"
points = gpx_load.feed(rwgps_url, delta=50)

log_time("gpx read")

feed_msgs = spot.feed("0VMqdoAtvZtPTNMNZJVZa5jhHdej3zLqM")

log_time("spot read")

points = [ spot.msg_to_latlon(pt) for pt in feed_msgs ]

log_time("spot convert")

report_times()
