#! /usr/bin/env python3
#
import sys
import os
import cgi
import json
import cgitb
from config_local import MONGO_URL
import datetime

# Pymongo is a local install
sys.path.append("./lib")

from pymongo import MongoClient

print("Content-type: text/plain")
print("")
# cgitb.enable()

time_format = "%Y-%m-%d %H:%M:%S%z"

def datetime_from_string( s ):
        """
        From the format that str(datetime.datetime.now()) produces
        Args:
           s: A string that looks like '2015-04-10 17:01:14+0000'
        Returns:
           a corresponding datetime.datetime object
        """
        try:
                dt = datetime.datetime.strptime(s, time_format)
        except ValueError:
                print("Unable to parse timestamp: {}"
                      .format(s, file=sys.stderr))
                dt = datetime.datetime.now(datetime.timezone.utc)
        return dt


form = cgi.FieldStorage()
feedlist = form.getlist('feed')

# print("Feeds requested: {}".format(feedlist), file=log)

featurelist = [ ]
geojson = { "type": "FeatureCollection",
            "features": featurelist
            }

client = MongoClient(MONGO_URL)
db = client.tracks
collection = db.samples

now = datetime.datetime.now(datetime.timezone.utc)
nowstring = now.strftime(time_format)
query_interval = datetime.timedelta(minutes=2)

stale = [ ]

for feed in feedlist:
    request = { "id": feed }
    record = collection.find_one(request)
    #print("Result of query: {}".format(record))
    if (record == None):
        record = { "id": feed,
                   "last_query_time": nowstring,
                            "messages": [ ]
                                  }
        collection.insert(record)
        #print("No record for {}".format(feed))
        stale.append(feed)
        continue

    last_queried = datetime_from_string(record["last_query_time"])
    if (now - last_queried) > query_interval:
        stale.append(feed)
        collection.update_one( {"id": feed },
                                {"$set": { "last_query_time": nowstring }})

    points = record["messages"]
    if points == [ ] : continue  # Skip unavailable feeds 
    latest = points[0]

    # Trajectory over a given period or number of observations ... number 
    # of observations for now.
    trajectory = [ ]
    if len(points) > 1: 
        recent = points[0:6]  # Could be less than 6 observations
        for point in recent: 
            trajectory.append( [ point["latitude"], point["longitude"]]  )
        feature = { "type": "Feature", 
                    "geometry": {
                          "type": "LineString",
                           "coordinates": trajectory
                     }, 
                     "properties": {
                          "id": feed, 
                          "show_on_map": "true"
             }
        }
        featurelist.append(feature)  
    
    feature = { "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates" : [ latest["latitude"], latest["longitude"] ],
                    },
                "properties": {
                    "id": feed,
                    "observed_time": latest["dateTime"], 
                    "show_on_map": "true",
                    "name": "Spotted", 
                  }
                }
    featurelist.append(feature)



#print("Featurelist returned: {}".format(geojson),file=log)

print(json.dumps(geojson))

os.system("python3 spot_to_mongo.py {} &".format(
                " ".join(stale)))

