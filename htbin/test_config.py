"""
A simple smoke-test of configuration for running Python scripts
in the 'enroute' rider tracking service.  We check for:
- Import of the libraries that we depend on
- Access to the MongoDB database
- Basic function of the GPX file loading and Spot service

Invoke this script from the command line.  If (or when) it works,
we can do further testing through the CGI interface. 
"""

print("Importing local scripts, which may indirectly import other libraries ...")
print("Note, libraries may be installed in ./lib; Python scripts")
print("should set sys.path appropriately")

import gpx_load

print("Importing known Python libraries directly")
### These need installing
import gpxpy
import pymongo
### These should be part of standard Python installation
from config_local import MONGO_URL
from pymongo import MongoClient
import urllib.request
import json
import codecs
print("Done with imports.")

print("Checking SPOT service and MongoDB interface with sample id from M Young")

feed_id = "0GiLP5jn9iVj8z8qm90QaTnkpygdAmouk" # Michal Young's Spot

print("First check database ... ")
client = MongoClient(MONGO_URL)
db = client.tracks
collection = db.samples
request = { "id": feed_id }
record = collection.find_one(request)
print("Result of query: {}...".format(record)[0:60])

print("Then try SPOT web service ...")
URL_API = "https://api.findmespot.com/spot-main-web/consumer/rest-api/2.0/public/feed/"
URL_REQUEST_RECENT = "/message"
URL= URL_API + feed_id + URL_REQUEST_RECENT
response = urllib.request.urlopen(URL)
txt = response.read().decode("utf-8")
data=json.loads(txt)
if "errors" in data["response"]:
    if empty_exception:
        msg = data["response"]["errors"]["error"]["description"]
        raise Exception(msg)
    else:
        print("Got an error response")
print("Response: {}...".format(txt[0:60]))



