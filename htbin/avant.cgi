#! /usr/bin/env python3
#
# Create bookmarkable site with tracking of multiple spots against
# rwgps map 

#
# Temporary hack: Dump as plain text to see what we
# will send to the browser later
# 
# print("Content-type: text/plain\n\n")
import cgi,cgitb # Before anything ... 
print("Content-type: text/html\n\n")
cgitb.enable() # Debugging on 

import os       # So we can look at environment variables
import random   # to generate a temporary URL
import os.path  # to get template files relative to script location
import urllib.parse # to locate htbin relative to current url
import spot  # Interface to Spot satellite tracker web service
import gpx_load


def random_url(length=6):
    """
    Return a random string of URLish characters.
    Choose length long enough to make collisions unlikely.
    """
    url_chars = 'abcdefghijklmnopqrstuvwxyz'
    return ''.join(random.choice(url_chars) for i in range(length))


def decode_env():
    """
    Extract what we need from the environment
    loading it up into a dict that is returned. 
    """
    whereami = os.environ["SCRIPT_FILENAME"]
    scriptdir = os.path.dirname(whereami)
    this_url = os.environ["REQUEST_URI"]
    url_parts = urllib.parse.urlparse(this_url)
    server = url_parts.scheme + url_parts.netloc + url_parts.path
    last_slash = server.rfind("/")
    server = server[:last_slash+1]
    url_key = random_url()
    fpath = "{}/../tracks/{}.html".format(scriptdir,url_key)
    target_url = "tracks/{}.html".format(url_key)
    env = { "server": server, 
    	    "fpath": fpath,
             "url": target_url
	    }
    return env

def decode_form():
    """
    The things we need from the form are the rwgps route#,
    a list of tracks, and a corresponding lists of names to
    associate with the tracks.  These are delivered in the
    form of a dict.

    If something is wrong, we'll return a dict with just
    one field, an error message. 
    """
    form = cgi.FieldStorage()
    if 'route' not in form or 'track' not in form: 
        return { "msg":
                 "<p>Form requires route# and at least one Spot track ID</p>" }
    route = form.getvalue('route')
    if ('name' in form):
        names = form.getlist("name")
    else:
        names = [ ]
    tracks = form.getlist("track")

    if len(tracks) > len(names):
        names += ["???", "???", "???", "???"]
    
    return { "route": route, "tracks": tracks, "names": names }


env = decode_env()
print(env)
form = decode_form()
print(form)
f = open(env["fpath"], mode="w")
print("<p>Created html!</p>", file=f)




