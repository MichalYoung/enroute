#! /usr/bin/env python3
#
import sys
import json
from config import MONGO_URL
import datetime
from pymongo import MongoClient

time_format = "%Y-%m-%d %H:%M:%S%z"

def datetime_from_string( s ):
        """
        From the format that str(datetime.datetime.now()) produces
        Args:
           s: A string that looks like '2015-04-10 08:32:42.041775'
        Returns:
           a corresponding datetime.datetime object
        """
        try:
                dt = datetime.datetime.strptime(s, time_format)
        except ValueError:
                print("Unable to parse timestamp: {}"
                      .format(s, file=sys.stderr))
                dt = datetime.datetime.now(datetime.timezone.utc)
        return dt


feedlist = sys.argv[1:]

now = datetime.datetime.now()
nowstring = now.strftime(time_format)
query_interval = datetime.timedelta(minutes=2)

client = MongoClient(MONGO_URL)
db = client.tracks
collection = db.samples

for feed in feedlist:
    request = { "id": feed }
    record = collection.find_one(request)
    if (record == None):
        print("No record for {}".format(feed))
        stale.append(feed)
        continue

    last_queried_string = record["last_query_time"]
    last_queried = datetime_from_string(record["last_query_time"])
    print("Record {} queried at {}  ({})".format(feed,last_queried_string,last_queried))
    if (now - last_queried) > query_interval:
	    print("Stale")
