"""
Constructing bits and pieces of Google Maps code
from data.
"""

####
# Google maps code
####
MAP_DOC_PREFIX = """<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
      html, body, #map-canvas { height: 100%; margin: 0; padding: 0;}
    </style>
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&key=AIzaSyBawx80ejfRk1xK9NCLFbHXa0RGimDWDKo">
    </script>
    <script type="text/javascript">
      function initialize() {
        var mapOptions = {
	center: { lat: 44.02239,   lng: -123.12459},
          zoom: 8
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);
   /* End of fixed prefix */ 
"""

MAP_DOC_SUFFIX = """
      /* Beginning of fixed suffix */ 
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  </head>
  <body>
<div id="map-canvas"></div>
  </body>
</html>
"""

###
### Naming standard:  g_X(d) means return code for creating
####    the google.maps.X thing from data d
###

SymCount = 0 # For generating variable names

def gensym(prefix):
    global SymCount
    SymCount += 1
    return prefix + str(SymCount)

def g_LatLng(pt):
    """
    Create google LatLng object from (lat, lon)
    """
    lat, lon = pt
    return "new google.maps.LatLng({}, {})".format(lat, lon)

def g_Polyline(pts,color="#FF0000",opacity=1):
    """
    Create polyline from an array of (lat,lon)
    """
    line_coords = gensym("coords_")
    line_name = gensym("line_")
    vertices = [ ]
    for latlon in map(g_LatLng, pts):
        vertices.append(latlon)
    points = ", \n\t\t".join(vertices)
    decl = """
          var {coords} = [
                {points}
          ];
          var {name} = new google.maps.Polyline({{
              path: {coords}, 
              geodesic: true,
              strokeColor: '{color}',
              strokeOpacity: {opacity},
              strokeWeight: {weight}
           }});
          {name}.setMap(map);
    """.format(name=line_name,coords=line_coords,points=points,
               weight=2,color=color,opacity=opacity)
    return decl
    
def g_Marker(pt,title="Last seen"):
    """
    A google "pin" marker
    """
    template = """
        var marker = new google.maps.Marker({{
           position:  {pos}, 
           map: map,
           title: "{title}"
         }});
         marker.setMap(map);
       """.format(pos=g_LatLng(pt),title=title)
    return template


    
# maps_url = "http://www.google.com/maps/@{},{}".format(lat,lon)
# print("Location: {}\n\n".format(maps_url))



    
