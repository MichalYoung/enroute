#! /usr/local/bin/python3
#
#  Phase 1 of enroute: Fetch, simplify, and save route
#  from GPX into a form that will be faster for us to
#  retrieve and display
#
import cgi,cgitb
import gpx_load

cgitb.enable() # Debugging on 
#
# Form data
# 
form = cgi.FieldStorage()
route = form.getvalue('route')
# route = "762903" # for testing

rwgps_url = "http://ridewithgps.com/routes/" + route.strip() + ".gpx"

# GPX route 
points = gpx_load.feed(rwgps_url, delta=50)
stash = open("../routes/{}".format(route),"w")
for pt in points:
    lat, lon = pt
    print("{} {}".format(lat,lon), file=stash)
stash.close()

print("Content-type: text/html\n\n")
content= """
<html>
<head>  <title>Stashed stuff</title>
</head>
<body>
  <p>The route has been saved.</p>
</body>
</html>
"""

print(content)




