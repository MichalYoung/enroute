"""
Configurable parameters for the Spot tracking project.

This is intended to be a central place for keeping information
that may vary from installation to installation, such as
information for reaching a MongoDB database on a particular
host at a particular port.  Import this into any Python script
that depends on that information.  While a default version
of this file may be kept under version control, each
installation will need its own copy with local changes.

Author: michal.young@gmail.com, February 2015
"""

#  Host and port of MongoDB database, which must be installed separately.
# 
MONGO_URL = "mongodb://localhost:27017"   # Default is local database, standard port

