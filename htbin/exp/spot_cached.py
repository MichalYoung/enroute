#! /usr/bin/env python3
"""
From spot_geojson.py, with addition of caching in MongoDB database
"""
import sys
import json
from time import sleep
#
# Additions for caching: 
from config import MONGO_URL
import datetime
from pymongo import MongoClient

feedlist = [ "0GiLP5jn9iVj8z8qm90QaTnkpygdAmouk" ]
featurelist = [ ]

geojson = { "type": "FeatureCollection",
                        "features": featurelist
                        }


def datetime_from_string( s ):
        """
        Convert from Spot time format 2015-02-17T15:47:27+0000
        (UTC with timezone added?)
        to Python datetime.datetime.
        Args:
           s: A string that looks like 2015-02-17T15:47:27+0000
        Returns:
           a corresponding datetime.datetime object
        """
        format = "%Y-%m-%dT%H:%M:%S%z"
        try:
                dt = datetime.datetime.strptime(s, format)
        except ValueError:
                print("Unable to parse timestamp: {}".format(s, file=sys.stderr))
                dt = datetime.datetime.now(datetime.timezone.utc)
        return dt

## Tactic: 
##        We always return data from the database, if present.  
##        If data is not in the database, or if it is stale, we 
##        mark the database record to note our intent to read, 
##        then we read, then we update the database. 
##
## Oops ... what I want to do is to return data and *then* update the 
##      database, but I'm not sure that will avoid the delay of waiting 
##      for tracks from teh Spot service.  The client might be stalled 
##      waiting for us to close stdout even if we've sent all the data.  That 
##      may be addressed later by moving from cgi-bin execution to flask. 
##

stale = [ ]      # Feeds we should refresh with calls from 

client = MongoClient(MONGO_URL)
db = client.tracks
collection = db.samples

for feed in feedlist: 
   request = { "id": feed }
   record = collection.find_one(request)
   print("Result of query: {}".format(record))
   nowstring = datetime.datetime.isoformat(datetime.datetime.now(
       datetime.timezone.utc))
   if (record == None):
                record = { "id": feed,
                            "last_query_time": nowstring,
                            "messages": featurelist
                                  }
                stale.append(feed)
                collection.insert(record)
                print("No record for {}".format(feed))
   else:
                observed = record["last_query_time"]
                staleness = datetime_from_string( observed ) - datetime.datetime.now(datetime.timezone.utc)
                print("Record for {} is stale by {}".format(feed, staleness))


