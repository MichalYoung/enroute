"""
Test of mongodb access from Python.

"""
import sys
import datetime
import pymongo
from config import MONGO_URL

# Time format: 
def datetime_from_string( s ):
    """
    Convert from Spot time format 2015-02-17T15:47:27+0000
    (UTC with timezone added?)
    to Python datetime.datetime.
    Args:
       s: A string that looks like 2015-02-17T15:47:27+0000
    Returns:
       a corresponding datetime.datetime object
    """
    format = "%Y-%m-%dT%H:%M:%S%z"
    try:
        dt = datetime.datetime.strptime(s, format)
    except ValueError:
        print("Unable to parse timestamp:", file=sys.stderr)
        dt = datetime.now()
    return dt



client = pymongo.MongoClient(MONGO_URL)
db = client.tracks_db
for track in db.tracks.find():
    print(track)
    observed = datetime_from_string(track["observed_time"])
    print("Last observed: {}".format(observed))
    print("Staleness: {}".format(datetime.datetime.now(datetime.timezone.utc) - observed))

    
