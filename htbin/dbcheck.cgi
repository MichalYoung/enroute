#! /bin/bash
#
echo "Content-type: text/plain"
echo ""
#
echo "Looking for Mongo.  Mongo, you there?"
#
# ps and grep in two steps so that grep doesn't see itself in process list
PROCS="/tmp/processes-$$.log"
ps -x > $PROCS
if (grep --silent "mongod" $PROCS); then
    echo "Mongo already alive.  Mongo happy about that"; 
else    
    echo "No find Mongo";
    echo "Making new Mongo instead";
    sh ~myoung/bin/mongo_go.sh
    echo "Maybe Mongo ok now?"
fi;
